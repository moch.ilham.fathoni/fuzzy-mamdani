<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::prefix('/admin')->name('admin.')->group(function(){
    Route::namespace('Auth')->group(function(){
        //Admin Login Routes
        Route::get('/login','AdminLoginController@showLoginForm')->name('login');
        Route::post('/login','AdminLoginController@login')->name('dologin');
        Route::post('/logout','AdminLoginController@logout')->name('logout');

        //Admin Forgot Password Routes
        Route::get('/password/reset','AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/email','AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');

        //Admin Reset Password Routes
        Route::get('/password/reset/{token}','AdminResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset','AdminResetPasswordController@reset')->name('password.update');

    });
    Route::get('/home', 'Admin\HomeController@index')->name('home');
    Route::resources([
        'user' => 'Admin\UserAdminController',
        'direktur' => 'Admin\UserDirekturController',
        'siswa' => 'Admin\SiswaController',
    ]);
    Route::get('/userdata', 'Admin\UserAdminController@getUser')->name('get.user');
    Route::get('/userdirektur', 'Admin\UserDirekturController@getUser')->name('get.direktur');
    Route::get('/datasiswa', 'Admin\SiswaController@getSiswa')->name('get.siswa');
    Route::match(['put', 'patch'], '/penilaian/{id}', 'Admin\SiswaController@penilaian')->name('penilaian');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/laporan', 'Admin\LaporanController@indexPenilaian')->name('laporan');
Route::get('/rekomendasi', 'Admin\LaporanController@indexRekomendasi')->name('rekomendasi');
Route::get('/datapenilaian', 'Admin\LaporanController@getPenilaianSiswa')->name('get.penilaian');
