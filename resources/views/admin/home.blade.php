@extends('layouts.lte')

@section('mp')
active
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Siswa</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Manajemen Penilaian</a></li>
                    <li class="breadcrumb-item active">Siswa</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            @if (session('status'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable custom-success-box">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> {{ session('status') }} </strong>
                </div>
            </div>
            @elseif (session('error'))
            <div class="col-md-12">
                <div class="alert alert-error alert-dismissable custom-error-box">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> {{ session('error') }} </strong>
                </div>
            </div>
            @endif

            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Siswa</h3>
                        <div class="card-tools">
                            <ul class="nav nav-pills ml-auto">
                                <li class="nav-item">
                                    <a class="btn btn-block btn-success btn-sm" href="{{ route('admin.siswa.create') }}">Tambah</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIS</th>
                                        <th>Nama Sekolah</th>
                                        <th>BK</th>
                                        <th>Foto</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
function deleteFunction(click) {
    if(confirm('Are you sure you want to delete this ?')) {
        click.parentNode.submit();
        return false;
    } else {
        return false;
    }
}
$(function() {
    $('#myTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.get.siswa') }}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', width: '5%', searchable: false },
            { data: 'name', name: 'name' },
            { data: 'nis', name: 'nis' },
            { data: 'sekolah', name: 'sekolah', width: '20%' },
            { data: 'bk', name: 'bk' },
            { data: 'foto', name: 'foto', orderable: false, searchable: false },
            { data: 'action', name: 'action', width: '20%', orderable: false, searchable: false }
        ]
    });
});
</script>
@endpush
