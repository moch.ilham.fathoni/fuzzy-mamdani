@extends('layouts.lte')

@section('open')
menu-open
@endsection

@section('users')
active
@endsection

@section('direktur')
active
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">

    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            @if (session('status'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable custom-success-box">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> {{ session('status') }} </strong>
                </div>
            </div>
            @elseif (session('error'))
            <div class="col-md-12">
                <div class="alert alert-error alert-dismissable custom-error-box">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> {{ session('error') }} </strong>
                </div>
            </div>
            @elseif ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="col-md-12">
                        <div class="alert alert-error alert-dismissable custom-error-box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> {{ $error }} </strong>
                        </div>
                    </div>
                @endforeach
            @endif

            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Edit User Direktur</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ route('admin.direktur.update', $user->id) }}">
                        @method('PATCH')
                        @csrf            
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="Masukkan nama" value="{{ $user->name }}" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email"
                                    placeholder="Masukkan email" value="{{ $user->email }}" required>
                            </div>
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" name="username"
                                    placeholder="Username" value="{{ $user->username }}" required>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <a class="btn btn-default float-right" href="{{ route('admin.direktur.index') }}">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
