@extends('layouts.lte')

@section('mp')
active
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">

    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            @if (session('status'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable custom-success-box">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> {{ session('status') }} </strong>
                </div>
            </div>
            @elseif (session('error'))
            <div class="col-md-12">
                <div class="alert alert-error alert-dismissable custom-error-box">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> {{ session('error') }} </strong>
                </div>
            </div>
            @elseif ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="col-md-12">
                        <div class="alert alert-error alert-dismissable custom-error-box">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong> {{ $error }} </strong>
                        </div>
                    </div>
                @endforeach
            @endif

            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Add Siswa</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ route('admin.siswa.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="Masukkan nama siswa" required>
                            </div>
                            <div class="form-group">
                                <label for="nis">Nomer Induk Siswa</label>
                                <input type="text" class="form-control" id="nis" name="nis"
                                    placeholder="Masukkan NIS Siswa" required>
                            </div>
                            <div class="form-group">
                                <label for="sekolah">Nama Sekolah</label>
                                <input type="text" class="form-control" id="sekolah" name="sekolah"
                                    placeholder="Masukkan Nama Sekolah" required>
                            </div>
                            <div class="form-group">
                                <label for="bk">Bidang Keahlian</label>
                                <input type="text" class="form-control" id="bk" name="bk"
                                    placeholder="Masukkan Bidang Keahlian" required>
                            </div>
                            <div class="form-group">
                                <label for="foto">Foto</label>
                                <input type="file" class="form-control" id="foto" name="foto" accept="image/*">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <a class="btn btn-default float-right" href="{{ route('admin.home') }}">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
