@extends('layouts.lte')

@section('mp')
active
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        @if ($errors->any())
            <h4 style="color:red;">Terjadi kesalahan pada sistem :</h1>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="invoice p-3 mb-3">
            <div id="printArea">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-2">
                            Nama
                        </div>
                        <div class="col-10">
                            : {{ $siswa->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            Nis
                        </div>
                        <div class="col-10">
                            : {{ $siswa->nis }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            Nama Sekolah
                        </div>
                        <div class="col-10">
                            : {{ $siswa->sekolah }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            Bidang Keahlian
                        </div>
                        <div class="col-10">
                            : {{ $siswa->bk }}
                        </div>
                    </div>
                </div>
            </div>
            <p></p>
            <form method="POST" action="{{ route('admin.penilaian', $siswa->id) }}">
            @method('PATCH')
            @csrf
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-sm table-bordered border-dark">
                        <thead>
                            <tr>
                                <th rowspan="2" style="vertical-align : middle;text-align:center;">BAGIAN</th>
                                <th rowspan="2" style="vertical-align : middle;text-align:center;">ASPEK PENILAIAN</th>
                                <th colspan="2" style="vertical-align : middle;text-align:center;">PENILAIAN</th>
                                <th rowspan="2" style="width: 20%; vertical-align: middle; text-align: center;">JUMLAH SKOR</th>
                            </tr>
                            <tr>
                                <td style="width: 15%; vertical-align: middle; text-align:center;">BOBOT<br>NILAI</td>
                                <td style="width: 15%; vertical-align: middle; text-align:center;">SKORE</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="background-color:#8673FF">
                                <td style="vertical-align : middle;text-align:center;">A</td>
                                <td style="vertical-align : middle;text-align:center;">Aspek Teknis</td>
                                <td style="vertical-align : middle;text-align:center;"><div id="nilaiA"></div></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="background-color:#54A7FF">
                                <td style="vertical-align : middle;text-align:center;">A1</td>
                                <td style="vertical-align : middle;text-align:center;">Keterampilan Dasar</td>
                                <td style="vertical-align : middle;text-align:center;"><div id="nilaiA1"></div></td>
                                <td></td>
                                <td style="vertical-align : middle;text-align:center;"><div id="jumlahA1"></div></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">1</td>
                                <td>Tata letak gambar</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a1_1_bobot }}" id="a1_1_bobot" name="a1_1_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_1_skore, 2) }}" id="a1_1_skore" name="a1_1_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_1_jumlah_skore, 2) }}" id="a1_1_jumlah_skore" name="a1_1_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">2</td>
                                <td>Konstruksi garis</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a1_2_bobot }}" id="a1_2_bobot" name="a1_2_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_2_skore, 2) }}" id="a1_2_skore" name="a1_2_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_2_jumlah_skore, 2) }}" id="a1_2_jumlah_skore" name="a1_2_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">3</td>
                                <td>Ukuran angka, huruf dan simbol</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a1_3_bobot }}" id="a1_3_bobot" name="a1_3_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_3_skore, 2) }}" id="a1_3_skore" name="a1_3_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_3_jumlah_skore, 2) }}" id="a1_3_jumlah_skore" name="a1_3_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">4</td>
                                <td>Etiket gambar</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a1_4_bobot }}" id="a1_4_bobot" name="a1_4_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_4_skore, 2) }}" id="a1_4_skore" name="a1_4_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a1_4_jumlah_skore, 2) }}" id="a1_4_jumlah_skore" name="a1_4_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="background-color:#54A7FF">
                                <td style="vertical-align : middle;text-align:center;">A2</td>
                                <td style="vertical-align : middle;text-align:center;">Gambar Penjelas</td>
                                <td style="vertical-align : middle;text-align:center;"><div id="nilaiA2"></div></td>
                                <td></td>
                                <td style="vertical-align : middle;text-align:center;"><div id="jumlahA2"></div></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">1</td>
                                <td>Konstruksi</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a2_1_bobot }}" id="a2_1_bobot" name="a2_1_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_1_skore, 2) }}" id="a2_1_skore" name="a2_1_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_1_jumlah_skore, 2) }}" id="a2_1_jumlah_skore" name="a2_1_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">2</td>
                                <td>Skala gambar</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a2_2_bobot }}" id="a2_2_bobot" name="a2_2_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_2_skore, 2) }}" id="a2_2_skore" name="a2_2_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_2_jumlah_skore, 2) }}" id="a2_2_jumlah_skore" name="a2_2_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">3</td>
                                <td>Simbol gambar</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a2_3_bobot }}" id="a2_3_bobot" name="a2_3_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_3_skore, 2) }}" id="a2_3_skore" name="a2_3_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_3_jumlah_skore, 2) }}" id="a2_3_jumlah_skore" name="a2_3_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">4</td>
                                <td>Detail Konstruksi</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a2_4_bobot }}" id="a2_4_bobot" name="a2_4_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_4_skore, 2) }}" id="a2_4_skore" name="a2_4_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a2_4_jumlah_skore, 2) }}" id="a2_4_jumlah_skore" name="a2_4_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="background-color:#54A7FF">
                                <td style="vertical-align : middle;text-align:center;">A3</td>
                                <td style="vertical-align : middle;text-align:center;">Gambar Bukaan (Isometrik)</td>
                                <td style="vertical-align : middle;text-align:center;"><div id="nilaiA3"></div></td>
                                <td></td>
                                <td style="vertical-align : middle;text-align:center;"><div id="jumlahA3"></div></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">1</td>
                                <td>Konstruksi</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a3_1_bobot }}" id="a3_1_bobot" name="a3_1_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_1_skore, 2) }}" id="a3_1_skore" name="a3_1_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_1_jumlah_skore, 2) }}" id="a3_1_jumlah_skore" name="a3_1_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">2</td>
                                <td>Kejelasan gambar</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a3_2_bobot }}" id="a3_2_bobot" name="a3_2_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_2_skore, 2) }}" id="a3_2_skore" name="a3_2_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_2_jumlah_skore, 2) }}" id="a3_2_jumlah_skore" name="a3_2_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">3</td>
                                <td>Skala gambar</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a3_3_bobot }}" id="a3_3_bobot" name="a3_3_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_3_skore, 2) }}" id="a3_3_skore" name="a3_3_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_3_jumlah_skore, 2) }}" id="a3_3_jumlah_skore" name="a3_3_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">4</td>
                                <td>Detail Konstruksi</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->a3_4_bobot }}" id="a3_4_bobot" name="a3_4_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_4_skore, 2) }}" id="a3_4_skore" name="a3_4_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->a3_4_jumlah_skore, 2) }}" id="a3_4_jumlah_skore" name="a3_4_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="background-color:#8673FF">
                                <td style="vertical-align : middle;text-align:center;">B</td>
                                <td style="vertical-align : middle;text-align:center;">Aspek Non Teknis</td>
                                <td style="vertical-align : middle;text-align:center;"><div id="nilaiB"></div></td>
                                <td></td>
                                <td style="vertical-align : middle;text-align:center;"><div id="jumlahB"></div></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">B1</td>
                                <td>Disiplin</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->b1_bobot }}" id="b1_bobot" name="b1_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b1_skore, 2) }}" id="b1_skore" name="b1_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b1_jumlah_skore, 2) }}" id="b1_jumlah_skore" name="b1_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">B2</td>
                                <td>Kerja Sama</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->b2_bobot }}" id="b2_bobot" name="b2_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b2_skore, 2) }}" id="b2_skore" name="b2_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b2_jumlah_skore, 2) }}" id="b2_jumlah_skore" name="b2_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">B3</td>
                                <td>Inisiatif</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->b3_bobot }}" id="b3_bobot" name="b3_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b3_skore, 2) }}" id="b3_skore" name="b3_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b3_jumlah_skore, 2) }}" id="b3_jumlah_skore" name="b3_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">B4</td>
                                <td>Tanggung Jawab</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->b4_bobot }}" id="b4_bobot" name="b4_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b4_skore, 2) }}" id="b4_skore" name="b4_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b4_jumlah_skore, 2) }}" id="b4_jumlah_skore" name="b4_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;">B5</td>
                                <td>Kebersihan</td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ $siswa->b5_bobot }}" id="b5_bobot" name="b5_bobot" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b5_skore, 2) }}" id="b5_skore" name="b5_skore" step="0.01" required></td>
                                <td><input style="text-align: right; width: 100%;" type="number" value="{{ number_format($siswa->b5_jumlah_skore, 2) }}" id="b5_jumlah_skore" name="b5_jumlah_skore" step="0.01" required></td>
                            </tr>
                            <tr style="background-color:#686868">
                                <td style="vertical-align : middle;text-align:center;"></td>
                                <td style="vertical-align : middle;text-align:center;" colspan="3"><b>Total A+B</b></td>
                                <td style="vertical-align : middle;text-align:center;"><b><div id="total"></b></td>
                            </tr>
                            <tr>
                                <td style="vertical-align : middle;text-align:center;" colspan="5">
                                    <div class="row">
                                        <div class="col-12">
                                            &nbsp;
                                        </div>
                                        <div class="col-6">
                                            <p>
                                                <br>Predikat : <b>{{ $predikat }}</b> <br>
                                                86 - 100 &nbsp;A. Baik Sekali<br>
                                                70 - 85 &nbsp;&nbsp;&nbsp;B. Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                60 - 69 &nbsp;&nbsp;&nbsp;C. Cukup&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                50 - 59 &nbsp;&nbsp;&nbsp;D. Kurang&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </p>
                                        </div>
                                        <div class="col-6">
                                            <p style="text-align:center;">Sorong, {{ date("d F Y") }}<br>Di Buat Oleh
                                            </p>
                                        </div>
                                        <div class="col-6">
                                            <p>&nbsp;</p>
                                        </div>
                                        <div class="col-6">
                                            <p style="text-align:center;"><u>Khairul Umar Lombu, S. Ars</u><br>Pembimbing Lapangan
                                            </p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <p>&nbsp;</p>
                </div>
                <div class="col-6" style="text-align:right;">
                    <button type="button" class="btn btn-primary" onclick="printDiv()">Print</button>
                    &nbsp;
                    <button type="submit" class="btn btn-success">Simpan</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
    function printDiv() {
        var divContents = document.getElementById("printArea").innerHTML;
        var a = window.open('', '', '');
        a.document.write('<html>');
        a.document.write('<head>');
        a.document.write("<link rel='stylesheet' href='{{ asset("css/app.css") }}'>");
        a.document.write('</head>');
        a.document.write('<body>');
        a.document.write(divContents);
        a.document.write('</body></html>');
        a.document.close();
        a.print();
    }
</script>
<script>
$(function() {
    var nilaiA = document.getElementById("nilaiA");
    var nilaiA1 = document.getElementById("nilaiA1");
    var jumlahA1 = document.getElementById("jumlahA1");
    var nilaiA2 = document.getElementById("nilaiA2");
    var jumlahA2 = document.getElementById("jumlahA2");
    var nilaiA3 = document.getElementById("nilaiA3");
    var jumlahA3 = document.getElementById("jumlahA3");
    var nilaiB = document.getElementById("nilaiB");
    var jumlahB = document.getElementById("jumlahB");
    var total = document.getElementById("total");

    function setValue() {
        var a1bobot1 = parseInt(document.getElementById("a1_1_bobot").value);
        var a1bobot2 = parseInt(document.getElementById("a1_2_bobot").value);
        var a1bobot3 = parseInt(document.getElementById("a1_3_bobot").value);
        var a1bobot4 = parseInt(document.getElementById("a1_4_bobot").value);
        var sumNilaiA1 = a1bobot1 + a1bobot2 + a1bobot3 + a1bobot4;
        var a1jumlah1 = parseFloat(document.getElementById("a1_1_jumlah_skore").value);
        var a1jumlah2 = parseFloat(document.getElementById("a1_2_jumlah_skore").value);
        var a1jumlah3 = parseFloat(document.getElementById("a1_3_jumlah_skore").value);
        var a1jumlah4 = parseFloat(document.getElementById("a1_4_jumlah_skore").value);
        var sumJumlahA1 = a1jumlah1 + a1jumlah2 + a1jumlah3 + a1jumlah4;

        var a2bobot1 = parseInt(document.getElementById("a2_1_bobot").value);
        var a2bobot2 = parseInt(document.getElementById("a2_2_bobot").value);
        var a2bobot3 = parseInt(document.getElementById("a2_3_bobot").value);
        var a2bobot4 = parseInt(document.getElementById("a2_4_bobot").value);
        var sumNilaiA2 = a2bobot1 + a2bobot2 + a2bobot3 + a2bobot4;
        var a2jumlah1 = parseFloat(document.getElementById("a2_1_jumlah_skore").value);
        var a2jumlah2 = parseFloat(document.getElementById("a2_2_jumlah_skore").value);
        var a2jumlah3 = parseFloat(document.getElementById("a2_3_jumlah_skore").value);
        var a2jumlah4 = parseFloat(document.getElementById("a2_4_jumlah_skore").value);
        var sumJumlahA2 = a2jumlah1 + a2jumlah2 + a2jumlah3 + a2jumlah4;

        var a3bobot1 = parseInt(document.getElementById("a3_1_bobot").value);
        var a3bobot2 = parseInt(document.getElementById("a3_2_bobot").value);
        var a3bobot3 = parseInt(document.getElementById("a3_3_bobot").value);
        var a3bobot4 = parseInt(document.getElementById("a3_4_bobot").value);
        var sumNilaiA3 = a3bobot1 + a3bobot2 + a3bobot3 + a3bobot4;
        var a3jumlah1 = parseFloat(document.getElementById("a3_1_jumlah_skore").value);
        var a3jumlah2 = parseFloat(document.getElementById("a3_2_jumlah_skore").value);
        var a3jumlah3 = parseFloat(document.getElementById("a3_3_jumlah_skore").value);
        var a3jumlah4 = parseFloat(document.getElementById("a3_4_jumlah_skore").value);
        var sumJumlahA3 = a3jumlah1 + a3jumlah2 + a3jumlah3 + a3jumlah4;

        var bobotB1 = parseInt(document.getElementById("b1_bobot").value);
        var bobotB2 = parseInt(document.getElementById("b2_bobot").value);
        var bobotB3 = parseInt(document.getElementById("b3_bobot").value);
        var bobotB4 = parseInt(document.getElementById("b4_bobot").value);
        var bobotB5 = parseInt(document.getElementById("b5_bobot").value);
        var sumNilaiB = bobotB1 + bobotB2 + bobotB3 + bobotB4 + bobotB5;
        var jumlahB1 = parseFloat(document.getElementById("b1_jumlah_skore").value);
        var jumlahB2 = parseFloat(document.getElementById("b2_jumlah_skore").value);
        var jumlahB3 = parseFloat(document.getElementById("b3_jumlah_skore").value);
        var jumlahB4 = parseFloat(document.getElementById("b4_jumlah_skore").value);
        var jumlahB5 = parseFloat(document.getElementById("b5_jumlah_skore").value);
        var sumJumlahB = jumlahB1 + jumlahB2 + jumlahB3 + jumlahB4 + jumlahB5;

        nilaiA1.innerHTML = sumNilaiA1;
        jumlahA1.innerHTML = sumJumlahA1.toFixed(2);
        nilaiA2.innerHTML = sumNilaiA2;
        jumlahA2.innerHTML = sumJumlahA2.toFixed(2);
        nilaiA3.innerHTML = sumNilaiA3;
        jumlahA3.innerHTML = sumJumlahA3.toFixed(2);
        nilaiA.innerHTML = sumNilaiA1 + sumNilaiA2 + sumNilaiA3;
        nilaiB.innerHTML = sumNilaiB;
        jumlahB.innerHTML = sumJumlahB.toFixed(2);
        var sumTotal = sumJumlahA1 + sumJumlahA2 + sumJumlahA3 + sumJumlahB;
        total.innerHTML = sumTotal.toFixed(2);
    }

    setInterval(function () {
        setValue();
    }, 1000); // update data di web setiap 1 detik
});
</script>
@endpush