<ul class="nav nav-pills flex-column">
    <li class="nav-item">
        <a href="{{ route('admin.direktur.edit', $model->id) }}" class="nav-link">
            <i class="fas fa-edit"></i> <small>Edit</small>
        </a>
    </li>
    <li class="nav-item">
        <form action="{{ route('admin.direktur.destroy', $model->id)}}" method="post" id="form_delete">
            @csrf
            @method('DELETE')
            <a href="" onclick="return deleteFunction(this)" class="nav-link">
                <i class="fas fa-trash-alt"></i> <small>Hapus</small>
            </a>
        </form>
    </li>
</ul>
