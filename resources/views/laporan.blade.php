@extends('layouts.lte')

@section('laporan-open')
menu-open
@endsection

@section('laporan')
active
@endsection

@section('hasil-penilaian')
active
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Penilaian Siswa</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active">Penilaian</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Data Penilaian</h3>
                        <div class="card-tools">
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIS</th>
                                        <th>Nama Sekolah</th>
                                        <th>BK</th>
                                        <th>Foto</th>
                                        <th>Penilaian</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
$(function() {
    $('#myTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('get.penilaian') }}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', width: '5%', searchable: false },
            { data: 'name', name: 'name' },
            { data: 'nis', name: 'nis' },
            { data: 'sekolah', name: 'sekolah', width: '20%' },
            { data: 'bk', name: 'bk' },
            { data: 'foto', name: 'foto', orderable: false, searchable: false },
            { data: 'predikat', name: 'predikat' }
        ]
    });
});
</script>
@endpush
