<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a href="{{ route('admin.logout') }}" class="nav-link" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none !important;">
                @csrf
            </form>
        </li>
    </ul>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
    <img src="{{ asset('img/fuzzy.png') }}" alt="Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
    <span class="brand-text font-weight-light">Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="{{ asset('img/user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="{{ route('admin.home') }}" class="nav-link @yield('mp')">
                <i class="nav-icon fas fa-desktop"></i>
                <p>
                    Manajemen Penilaian
                </p>
                </a>
            </li>
            {{-- <li class="nav-item">
                <a href="#" class="nav-link @yield('md')">
                <i class="nav-icon fas fa-globe"></i>
                <p>
                    Manajemen Domain
                </p>
                </a>
            </li> --}}
            <li class="nav-item has-treeview @yield('laporan-open')">
                <a href="#" class="nav-link @yield('laporan')">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    Laporan
                    <i class="right fas fa-angle-left"></i>
                </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('laporan') }}" class="nav-link @yield('hasil-penilaian')">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Hasil Penilaian</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('rekomendasi') }}" class="nav-link @yield('siswa-direkrut')">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Siswa Direkrut</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview @yield('open')">
                <a href="#" class="nav-link @yield('users')">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Users
                    <i class="right fas fa-angle-left"></i>
                </p>
                </a>
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('admin.user.index') }}" class="nav-link @yield('admin')">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Admin</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.direktur.index') }}" class="nav-link @yield('direktur')">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Direktur</p>
                    </a>
                </li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>