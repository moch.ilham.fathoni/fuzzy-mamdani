<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'nis', 'sekolah', 'bk', 'foto', 'updated_at',
        'a1_1_bobot', 'a1_2_bobot', 'a1_3_bobot', 'a1_4_bobot',
        'a1_1_skore', 'a1_2_skore', 'a1_3_skore', 'a1_4_skore',
        'a1_1_jumlah_skore', 'a1_2_jumlah_skore', 'a1_3_jumlah_skore', 'a1_4_jumlah_skore',
        'a2_1_bobot', 'a2_2_bobot', 'a2_3_bobot', 'a2_4_bobot',
        'a2_1_skore', 'a2_2_skore', 'a2_3_skore', 'a2_4_skore',
        'a2_1_jumlah_skore', 'a2_2_jumlah_skore', 'a2_3_jumlah_skore', 'a2_4_jumlah_skore',
        'a3_1_bobot', 'a3_2_bobot', 'a3_3_bobot', 'a3_4_bobot',
        'a3_1_skore', 'a3_2_skore', 'a3_3_skore', 'a3_4_skore',
        'a3_1_jumlah_skore', 'a3_2_jumlah_skore', 'a3_3_jumlah_skore', 'a3_4_jumlah_skore',
        'b1_bobot', 'b2_bobot', 'b3_bobot', 'b4_bobot', 'b5_bobot',
        'b1_skore', 'b2_skore', 'b3_skore', 'b4_skore', 'b5_skore',
        'b1_jumlah_skore', 'b2_jumlah_skore', 'b3_jumlah_skore', 'b4_jumlah_skore', 'b5_jumlah_skore',
    ];
}
