<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Siswa;
use App\Http\Controllers\Admin\SiswaController;

class LaporanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin,web');
    }

    public function indexPenilaian()
    {
        return view('laporan');
    }

    public function indexRekomendasi()
    {
        return view('rekomendasi');
    }

    public function getPenilaianSiswa()
    {
        $siswa = Siswa::all();
        return Datatables::of($siswa)
                         ->addIndexColumn()
                         ->addColumn('predikat', function ($model) {
                            $rumus = new SiswaController();
                            $total = $model->a1_1_jumlah_skore + $model->a1_2_jumlah_skore + $model->a1_3_jumlah_skore + $model->a1_4_jumlah_skore +
                                     $model->a2_1_jumlah_skore + $model->a2_2_jumlah_skore + $model->a2_3_jumlah_skore + $model->a2_4_jumlah_skore +
                                     $model->a3_1_jumlah_skore + $model->a3_2_jumlah_skore + $model->a3_3_jumlah_skore + $model->a3_4_jumlah_skore +
                                     $model->b1_jumlah_skore + $model->b2_jumlah_skore + $model->b3_jumlah_skore + $model->b4_jumlah_skore + $model->b5_jumlah_skore;
                            return $rumus->fuzzy($total);
                         })
                         ->editColumn('foto', function ($model) {
                            return view('admin.helper.foto', compact('model'))->render();
                         })
                         ->rawColumns(['foto'])
                         ->make(true);
    }
}
