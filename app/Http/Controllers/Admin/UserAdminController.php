<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Admin;

class UserAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.useradmin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.form.createadmin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:50', 'unique:admins'],
            'username' => ['required', 'string', 'max:50', 'unique:admins'],
            'password' => ['required', 'string'],
        ]);
        try {
            Admin::create(['name'       => $request->get('name'),
                           'username'   => $request->get('username'),
                           'email'      => $request->get('email'),
                           'password'   => Hash::make($request->get('password')),
            ]);
            return redirect()->route('admin.user.index')->with('status', 'User admin telah berhasil dibuat!');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::find($id);
        return view('admin.form.editadmin', compact('user'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Admin::find($id);

        $request->validate([
            'name' => ['required', 'string', 'max:50'],
        ]);

        if ($user->email != $request->get('email')) {
            $request->validate([
                'email' => ['required', 'string', 'email', 'max:50', 'unique:users'],
            ]);
            $user->email = $request->get('email');
        }

        if ($user->username != $request->get('username')) {
            $request->validate([
                'username' => ['required', 'string', 'max:50', 'unique:users'],
            ]);
            $user->username = $request->get('username');
        }

        $user->name = $request->get('name');
        $user->updated_at = date("Y-m-d H:i:s");
        $user->save();

        $msg = 'Data user ' . $user->name . ' telah berhasil diubah!';
        return redirect()->route('admin.user.index')->with('status', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = Admin::find($id);
            $msg = 'User ' . $user->name . ' telah berhasil dihapus!';
            $user->delete();
            return back()->with('status', $msg);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function getUser()
    {
        $user = Admin::all();
        return Datatables::of($user)
                         ->addIndexColumn()
                         ->addColumn('action', function ($model) {
                            return view('admin.action.user', compact('model'))->render();
                         })
                         ->make(true);
    }
}
