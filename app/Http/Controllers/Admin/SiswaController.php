<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Siswa;

class SiswaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.form.createsiswa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'nis' => ['required', 'string'],
            'sekolah' => ['required', 'string'],
            'bk' => ['required', 'string'],
        ]);
        try {
            if ($request->hasFile('foto')) {
                $foto = $request->file('foto');
                $siswa = Siswa::create(['name'      => $request->get('name'),
                                        'nis'       => $request->get('nis'),
                                        'sekolah'   => $request->get('sekolah'),
                                        'bk'        => $request->get('bk'),
                                        'foto'      => $foto->getClientOriginalName(),
                         ]);
                $path = 'foto/siswa/' . $siswa->id;
                $foto->move($path, $foto->getClientOriginalName());
            } else {
                $siswa = Siswa::create(['name'      => $request->get('name'),
                                        'nis'       => $request->get('nis'),
                                        'sekolah'   => $request->get('sekolah'),
                                        'bk'        => $request->get('bk'),
                         ]);
            }

            return redirect()->route('admin.home')->with('status', 'Siswa telah berhasil ditambahkan');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siswa = Siswa::find($id);
        $total = $siswa->a1_1_jumlah_skore + $siswa->a1_2_jumlah_skore + $siswa->a1_3_jumlah_skore + $siswa->a1_4_jumlah_skore +
                 $siswa->a2_1_jumlah_skore + $siswa->a2_2_jumlah_skore + $siswa->a2_3_jumlah_skore + $siswa->a2_4_jumlah_skore +
                 $siswa->a3_1_jumlah_skore + $siswa->a3_2_jumlah_skore + $siswa->a3_3_jumlah_skore + $siswa->a3_4_jumlah_skore +
                 $siswa->b1_jumlah_skore + $siswa->b2_jumlah_skore + $siswa->b3_jumlah_skore + $siswa->b4_jumlah_skore + $siswa->b5_jumlah_skore;
        $predikat = $this->fuzzy($total);
        return view('admin.form.penilaian', compact('siswa', 'predikat'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::find($id);
        return view('admin.form.editsiswa', compact('siswa'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'nis' => ['required', 'string'],
            'sekolah' => ['required', 'string'],
            'bk' => ['required', 'string'],
        ]);

        $siswa = Siswa::find($id);
        $siswa->name = $request->get('name');
        $siswa->nis = $request->get('nis');
        $siswa->sekolah = $request->get('sekolah');
        $siswa->bk = $request->get('bk');
        $siswa->updated_at = date("Y-m-d H:i:s");
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $path = 'foto/siswa/' . $siswa->id;
	        $foto->move($path, $foto->getClientOriginalName());
            $siswa->foto = $foto->getClientOriginalName();
        }
        $siswa->save();

        $msg = 'Data siswa ' . $siswa->name . ' telah berhasil diubah!';
        return redirect()->route('admin.home')->with('status', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $siswa = Siswa::find($id);
            $msg = 'Siswa ' . $siswa->name . ' telah berhasil dihapus!';
            $siswa->delete();
            return back()->with('status', $msg);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function getSiswa()
    {
        $siswa = Siswa::all();
        return Datatables::of($siswa)
                         ->addIndexColumn()
                         ->addColumn('action', function ($model) {
                            return view('admin.action.siswa', compact('model'))->render();
                         })
                         ->editColumn('foto', function ($model) {
                            return view('admin.helper.foto', compact('model'))->render();
                         })
                         ->rawColumns(['foto', 'action'])
                         ->make(true);
    }

    public function penilaian(Request $request, $id)
    {
        $request->validate([
            'a1_1_bobot'        => ['required', 'integer'],
            'a1_2_bobot'        => ['required', 'integer'],
            'a1_3_bobot'        => ['required', 'integer'],
            'a1_4_bobot'        => ['required', 'integer'],
            'a1_1_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a1_2_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a1_3_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a1_4_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a1_1_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a1_2_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a1_3_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a1_4_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_1_bobot'        => ['required', 'integer'],
            'a2_2_bobot'        => ['required', 'integer'],
            'a2_3_bobot'        => ['required', 'integer'],
            'a2_4_bobot'        => ['required', 'integer'],
            'a2_1_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_2_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_3_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_4_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_1_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_2_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_3_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a2_4_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_1_bobot'        => ['required', 'integer'],
            'a3_2_bobot'        => ['required', 'integer'],
            'a3_3_bobot'        => ['required', 'integer'],
            'a3_4_bobot'        => ['required', 'integer'],
            'a3_1_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_2_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_3_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_4_skore'        => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_1_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_2_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_3_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'a3_4_jumlah_skore' => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b1_bobot'          => ['required', 'integer'],
            'b2_bobot'          => ['required', 'integer'],
            'b3_bobot'          => ['required', 'integer'],
            'b4_bobot'          => ['required', 'integer'],
            'b5_bobot'          => ['required', 'integer'],
            'b1_skore'          => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b2_skore'          => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b3_skore'          => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b4_skore'          => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b5_skore'          => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b1_jumlah_skore'   => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b2_jumlah_skore'   => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b3_jumlah_skore'   => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b4_jumlah_skore'   => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'b5_jumlah_skore'   => 'nullable|regex:/^\d*(\.\d{2})?$/',
        ]);

        $siswa = Siswa::find($id);
        $siswa->a1_1_bobot = $request->get('a1_1_bobot');
        $siswa->a1_2_bobot = $request->get('a1_2_bobot');
        $siswa->a1_3_bobot = $request->get('a1_3_bobot');
        $siswa->a1_4_bobot = $request->get('a1_4_bobot');
        $siswa->a1_1_skore = $request->get('a1_1_skore');
        $siswa->a1_2_skore = $request->get('a1_2_skore');
        $siswa->a1_3_skore = $request->get('a1_3_skore');
        $siswa->a1_4_skore = $request->get('a1_4_skore');
        $siswa->a1_1_jumlah_skore = $request->get('a1_1_jumlah_skore');
        $siswa->a1_2_jumlah_skore = $request->get('a1_2_jumlah_skore');
        $siswa->a1_3_jumlah_skore = $request->get('a1_3_jumlah_skore');
        $siswa->a1_4_jumlah_skore = $request->get('a1_4_jumlah_skore');

        $siswa->a2_1_bobot = $request->get('a2_1_bobot');
        $siswa->a2_2_bobot = $request->get('a2_2_bobot');
        $siswa->a2_3_bobot = $request->get('a2_3_bobot');
        $siswa->a2_4_bobot = $request->get('a2_4_bobot');
        $siswa->a2_1_skore = $request->get('a2_1_skore');
        $siswa->a2_2_skore = $request->get('a2_2_skore');
        $siswa->a2_3_skore = $request->get('a2_3_skore');
        $siswa->a2_4_skore = $request->get('a2_4_skore');
        $siswa->a2_1_jumlah_skore = $request->get('a2_1_jumlah_skore');
        $siswa->a2_2_jumlah_skore = $request->get('a2_2_jumlah_skore');
        $siswa->a2_3_jumlah_skore = $request->get('a2_3_jumlah_skore');
        $siswa->a2_4_jumlah_skore = $request->get('a2_4_jumlah_skore');

        $siswa->a3_1_bobot = $request->get('a3_1_bobot');
        $siswa->a3_2_bobot = $request->get('a3_2_bobot');
        $siswa->a3_3_bobot = $request->get('a3_3_bobot');
        $siswa->a3_4_bobot = $request->get('a3_4_bobot');
        $siswa->a3_1_skore = $request->get('a3_1_skore');
        $siswa->a3_2_skore = $request->get('a3_2_skore');
        $siswa->a3_3_skore = $request->get('a3_3_skore');
        $siswa->a3_4_skore = $request->get('a3_4_skore');
        $siswa->a3_1_jumlah_skore = $request->get('a3_1_jumlah_skore');
        $siswa->a3_2_jumlah_skore = $request->get('a3_2_jumlah_skore');
        $siswa->a3_3_jumlah_skore = $request->get('a3_3_jumlah_skore');
        $siswa->a3_4_jumlah_skore = $request->get('a3_4_jumlah_skore');

        $siswa->b1_bobot = $request->get('b1_bobot');
        $siswa->b2_bobot = $request->get('b2_bobot');
        $siswa->b3_bobot = $request->get('b3_bobot');
        $siswa->b4_bobot = $request->get('b4_bobot');
        $siswa->b5_bobot = $request->get('b5_bobot');
        $siswa->b1_skore = $request->get('b1_skore');
        $siswa->b2_skore = $request->get('b2_skore');
        $siswa->b3_skore = $request->get('b3_skore');
        $siswa->b4_skore = $request->get('b4_skore');
        $siswa->b5_skore = $request->get('b5_skore');
        $siswa->b1_jumlah_skore = $request->get('b1_jumlah_skore');
        $siswa->b2_jumlah_skore = $request->get('b2_jumlah_skore');
        $siswa->b3_jumlah_skore = $request->get('b3_jumlah_skore');
        $siswa->b4_jumlah_skore = $request->get('b4_jumlah_skore');
        $siswa->b5_jumlah_skore = $request->get('b5_jumlah_skore');
        $siswa->save();

        return back()->with('status', 'Data berhasil disimpan');
    }

    public function fuzzy($x)
    {
        $kurang = (59 - $x) / 9;

        $cukup1 = ($x - 60) / 9;
        $cukup2 = (69 - $x) / 1;
        $cukup = min($cukup1, $cukup2);

        $baik1 = ($x - 70) / 14;
        $baik2 = (84 - $x) / 1;
        $baik = min($baik1, $baik2);

        $sangatbaik = ($x - 86) / 15;

        $hasil = max($kurang, $cukup, $baik, $sangatbaik);

        if ($hasil == $kurang) {
            return 'D (Kurang)';
        }
        if ($hasil == $cukup) {
            return 'C (Cukup)';
        }
        if ($hasil == $baik) {
            return 'B (Baik)';
        }
        if ($hasil == $sangatbaik) {
            return 'A (Baik Sekali)';
        }

        return '';
    }
}
