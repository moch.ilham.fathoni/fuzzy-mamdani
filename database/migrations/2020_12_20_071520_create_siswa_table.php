<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('nis');
            $table->string('sekolah');
            $table->string('bk');
            $table->string('foto')->nullable();

            $table->integer('a1_1_bobot')->nullable()->default(0);
            $table->integer('a1_2_bobot')->nullable()->default(0);
            $table->integer('a1_3_bobot')->nullable()->default(0);
            $table->integer('a1_4_bobot')->nullable()->default(0);
            $table->float('a1_1_skore', 8, 2)->nullable()->default(0);
            $table->float('a1_2_skore', 8, 2)->nullable()->default(0);
            $table->float('a1_3_skore', 8, 2)->nullable()->default(0);
            $table->float('a1_4_skore', 8, 2)->nullable()->default(0);
            $table->float('a1_1_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a1_2_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a1_3_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a1_4_jumlah_skore', 8, 2)->nullable()->default(0);

            $table->integer('a2_1_bobot')->nullable()->default(0);
            $table->integer('a2_2_bobot')->nullable()->default(0);
            $table->integer('a2_3_bobot')->nullable()->default(0);
            $table->integer('a2_4_bobot')->nullable()->default(0);
            $table->float('a2_1_skore', 8, 2)->nullable()->default(0);
            $table->float('a2_2_skore', 8, 2)->nullable()->default(0);
            $table->float('a2_3_skore', 8, 2)->nullable()->default(0);
            $table->float('a2_4_skore', 8, 2)->nullable()->default(0);
            $table->float('a2_1_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a2_2_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a2_3_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a2_4_jumlah_skore', 8, 2)->nullable()->default(0);

            $table->integer('a3_1_bobot')->nullable()->default(0);
            $table->integer('a3_2_bobot')->nullable()->default(0);
            $table->integer('a3_3_bobot')->nullable()->default(0);
            $table->integer('a3_4_bobot')->nullable()->default(0);
            $table->float('a3_1_skore', 8, 2)->nullable()->default(0);
            $table->float('a3_2_skore', 8, 2)->nullable()->default(0);
            $table->float('a3_3_skore', 8, 2)->nullable()->default(0);
            $table->float('a3_4_skore', 8, 2)->nullable()->default(0);
            $table->float('a3_1_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a3_2_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a3_3_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('a3_4_jumlah_skore', 8, 2)->nullable()->default(0);

            $table->integer('b1_bobot')->nullable()->default(0);
            $table->integer('b2_bobot')->nullable()->default(0);
            $table->integer('b3_bobot')->nullable()->default(0);
            $table->integer('b4_bobot')->nullable()->default(0);
            $table->integer('b5_bobot')->nullable()->default(0);
            $table->float('b1_skore', 8, 2)->nullable()->default(0);
            $table->float('b2_skore', 8, 2)->nullable()->default(0);
            $table->float('b3_skore', 8, 2)->nullable()->default(0);
            $table->float('b4_skore', 8, 2)->nullable()->default(0);
            $table->float('b5_skore', 8, 2)->nullable()->default(0);
            $table->float('b1_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('b2_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('b3_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('b4_jumlah_skore', 8, 2)->nullable()->default(0);
            $table->float('b5_jumlah_skore', 8, 2)->nullable()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
