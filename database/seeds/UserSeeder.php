<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Direktur',
            'email' => 'direktur@gmail.com',
            'username' => 'direktur',
            'password' => bcrypt('12345678')
        ]);
    }
}
